import {TURNS} from "../constants.js";
import {resetGameStorage} from "./storage.js";

export const checkWinner = (board, winner) => {
    const boardSize = board.length;
    const xWinVal = TURNS.X.charCodeAt(0) * 3; //645;
    const oWinVal = TURNS.O.charCodeAt(0) * 3;//168384;

    let row, col, d1, d2;
    for (let i = 0; i < boardSize; i += 3) {
        row = board[i].charCodeAt(0) + board[i + 1].charCodeAt(0) + board[i + 2].charCodeAt(0);
        col = board[i / 3].charCodeAt(0) + board[i / 3 + 3].charCodeAt(0) + board[i / 3 + 6].charCodeAt(0);
        d1 = board[0].charCodeAt(0) + board[4].charCodeAt(0) + board[8].charCodeAt(0);
        d2 = board[2].charCodeAt(0) + board[4].charCodeAt(0) + board[6].charCodeAt(0);

        if (row === xWinVal || col === xWinVal || d1 === xWinVal || d2 === xWinVal) {
            return TURNS.X;

        } else if (row === oWinVal || col === oWinVal || d1 === oWinVal || d2 === oWinVal) {
            return TURNS.O;
        }
    }

    if (!board.includes(' ') && winner === null) return 'd';
}

export const resetGame = (setBoard, setTurn, setWinner) => {
    setBoard(Array(9).fill(' '));
    setTurn(TURNS.X);
    setWinner(null);

    resetGameStorage();
}