import {Square} from "./Square.jsx";

export function ConditionalWinner({winner, resetGame}) {
    if (winner === null) return;
    const winnerText = winner === 'd' ? 'draw' : `${winner} wins!`;
    return (
        <section className="winner">
            <div className="text">
                <h2>
                    {winnerText}
                </h2>

                {
                    winner !== 'd' &&
                    <header className="win">
                        <Square>{winner}</Square>
                    </header>
                }


                <footer>
                    <button onClick={resetGame}>Reset</button>
                </footer>
            </div>
        </section>
    )
}