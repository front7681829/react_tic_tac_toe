import './App.css'
import {Square} from "./components/Square.jsx";
import {useState} from "react";
import JSConfetti from 'js-confetti'
import {TURNS} from "../constants.js";
import {ConditionalWinner} from "./components/ConditionalWinner.jsx";
import {checkWinner, resetGame} from "../logic/game.js";
import {saveGameToStorage} from "../logic/storage.js";

function App() {
    const [board, setBoard] = useState(() => {
        const boardFromStorage = window.localStorage.getItem('board');
        return boardFromStorage ? JSON.parse(boardFromStorage) : Array(9).fill(' ');
    });
    const [turn, setTurn] = useState(() => {
        const turnFromStorage = window.localStorage.getItem('turn');
        return turnFromStorage ?? TURNS.X;
    });
    const [winner, setWinner] = useState(null); // null (no winner), 'd' (draw)

    const updateBoard = (index) => {
        if (board[index] === TURNS.X || board[index] === TURNS.O || winner) return;

        const newBoard = [...board];
        newBoard[index] = turn;
        setBoard(newBoard);

        const newTurn = turn === TURNS.X ? TURNS.O : TURNS.X;
        setTurn(newTurn);

        saveGameToStorage(newBoard, newTurn);

        const newWinner = checkWinner(newBoard, winner);
        if (newWinner) {
            const confetti = new JSConfetti()
            confetti.addConfetti({
                emojis: ['🌈', '⚡️', '💥', '✨', '💫', '🌸'],
            }).then(() => console.log('Confetti animation completed!'));
            setWinner(newWinner);
        }
    }

    return (
        <main className='board'>
            <h1>Tic Tac Toe</h1>
            <section className="game">
                {board.map((square, index) => {
                    return (
                        <Square
                            key={index}
                            index={index}
                            updateBoard={updateBoard}
                        >
                            {square}
                        </Square>
                    )
                })}
            </section>
            <section className="turn">
                <Square isSelected={turn === TURNS.X}>
                    {TURNS.X}
                </Square>

                <Square isSelected={turn === TURNS.O}>
                    {TURNS.O}
                </Square>
            </section>

            <ConditionalWinner
                winner={winner}
                resetGame={() => resetGame(setBoard, setTurn, setWinner)}
            ></ConditionalWinner>
        </main>

    )
}

export default App
